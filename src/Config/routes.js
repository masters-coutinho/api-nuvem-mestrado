const express = require('express')
const deviceAuth = require('../Device/deviceAuth')
const userAuth = require('../User/userAuth')

module.exports = function(server)
{
    const api = express.Router()
    server.use('/api',api)

    const openApi = express.Router()
    server.use('/oapi',openApi)
    
    const protectedApi = express.Router()
    server.use('/api',protectedApi)
    protectedApi.use(deviceAuth)
    protectedApi.use(userAuth)

    const deviceAuthService = require('../Device/deviceAuthService')
    openApi.post('/signup',deviceAuthService.signup)
    openApi.post('/login',deviceAuthService.login)
    openApi.post('/validateToken',deviceAuthService.validateToken)

    const userAuthService = require('../User/userAuthService')
    openApi.post('/user/signup',userAuthService.signup)
    openApi.post('/user/login',userAuthService.login)
    openApi.post('/user/validateToken',userAuthService.validateToken)


    const deviceService = require('../Device/deviceService')
    deviceService.register(protectedApi,'/device')

    const dadoService = require('../DadoTeste/dadoService')
    dadoService.register(protectedApi,'/dado')

    const tokenService = require('../Token/tokenService')
    tokenService.register(protectedApi,'/token')
}