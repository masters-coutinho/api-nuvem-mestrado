const restful = require('node-restful')
const mongoose = restful.mongoose

const tokenSchema = new mongoose.Schema({
    device:{type:String, unique:true},
    token:{type:String, unique:true}
})

module.exports = restful.model('Token',tokenSchema)