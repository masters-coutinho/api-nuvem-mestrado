const Dado = require('./dado')
const lodash = require('lodash')
let moment = require('moment')

Dado.methods(['get','delete'])
Dado.updateOptions({new:true, runValidators:true})

Dado.after('post', sendErrorsOrNext).after('put',sendErrorsOrNext)

function sendErrorsOrNext(req,resp,next){
    const bundle = resp.locals.bundle

    if(bundle.errors){
        var errors = parseErrors(bundle.errors)
        resp.status(500).json({errors})
    }else{
        next()
    }
}



function parseErrors(nodeRestfulErrors){

    const errors = []
    lodash.forIn(nodeRestfulErrors, error =>{
        errors.push(error.message)
    })
}

Dado.route('count',(req,resp,next) =>{
    Dado.count((err,value)=>{
        if(err){
            resp.status(500).json({errors:[err]})
        }else{
            resp.json(value)
        }
    })
})

Dado.route('fog-1s',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-1s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },{$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1 }
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-01s',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-01s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },{$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1 }
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-02s',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-02s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },{$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1 }
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-05s',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-05s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },{$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1 }
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-005s',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-005s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },{$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1 }
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-002s',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-002s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },{$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1 }
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-001s',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-001s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },
                {$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1}
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-india3',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-india3"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },
                {$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1}
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-lat',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "artNode10m/s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },
                {$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1}
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-india2',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-india2"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },
                {$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1}
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('fog-autenticate',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "fog-autenticate"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },{$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1 }
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('data',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "DATA"}
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

Dado.route('save',(req, resp, next)=>{
    
    const timestampInicial = req.body.timestampInicial || ''
    let timestampFinal = req.body.timestampFinal || ''
    let tempoTotal = req.body.timestampInicial || ''
    const deviceId = req.body.deviceId || ''
    const mensagem = req.body.mensagem || ''
    const codigo = req.body.codigo || ''

    timestampFinal = moment().valueOf()/1000
    tempoTotal = ((timestampFinal.valueOf())-(timestampInicial.valueOf()))


    const newDado = new Dado({
        codigo:codigo,
        timestampInicial : timestampInicial,
        timestampFinal : timestampFinal,
        tempoTotal : tempoTotal.valueOf(),
        deviceId : deviceId,
        mensagem : mensagem
    })


    console.log("DADO = "+ newDado)
    newDado.save(err => {
            if(err) {
                resp.send("ERRO ao Salvar")
            } else {
                  resp.send(newDado)
                }
            })
})


Dado.route('artMeu10ms',(req, resp, next)=>{
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: "artNode10m/s"}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },
                {$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1}
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})


Dado.route('codigo',(req, resp, next)=>{   
    let codigo = req.body.codigo || ''
    Dado.find((err, dado)=>{
        if(err) {
            return sendErrorsFromDB(res, err)
        }else if(dado){

            if(dado == "")
                return resp.status(400).send({errors: ['Não há dado.']})
            
            else{
                Dado.aggregate([
                    {$match: {codigo: codigo}},
                    {$group:{_id:null, media:{$avg:"$tempoTotal"}, 
                                       min:{$min:"$tempoTotal"},
                                       max:{$max:"$tempoTotal"},
                                       desvio:{$stdDevSamp:"$tempoTotal"},
                                       total: {$sum: 1}}
                    
                },
                {$project:{_id:0, 
                    media:1,
                    min:1,
                    max:1,
                    desvio:1,
                    total:1}
                
                }],function (err, result) {
                    if(err) {
                        return resp.status(800).json({errors:[err]})
                    }else{
                        resp.json(lodash.defaults(result[0]))
                    }
                })

            }
        }else{
            return resp.status(400).send({errors: ['Não há mensagem.']})
        }
    })
})

module.exports = Dado