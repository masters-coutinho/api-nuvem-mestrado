const restful = require('node-restful')
const mongoose = restful.mongoose

//03da950013be11ebab5874d4359afc5a

const deviceSchema = new mongoose.Schema({
    key:{type:String, unique:true},
    gatewayId:{type:String, unique:true}
})

module.exports = restful.model('Device',deviceSchema)